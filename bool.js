// const boolToWord = (bool) => {
//     if (bool == true) {
//         return 'Yes';
//     }
//     else return 'No';
// };

const boolToWord = (bool) => {
    return bool ? 'Yes' : 'No';
};

exports.boolToWord = boolToWord;