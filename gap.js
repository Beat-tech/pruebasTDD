const highAndLow= (numbers) => {
    var numbers = numbers.split(' ');
    
    return Math.max.apply(null, numbers) + " " + Math.min.apply(null, numbers);
  }

exports.highAndLow = highAndLow;