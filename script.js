const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/deportes/:seccion/:num_noticia?', (req, res) => {
  if (req.params.seccion && req.params.num_noticia) {
    console.log("Seccion: " + req.params.seccion)
    res.send(`Deportes. Seccion:` + req.params.seccion + `Num noticia:` + req.params.num_noticia)
  }
  })

app.get('/internacional/:pais/', (req, res) => {
  console.log(req.params.pais)
  res.send('Internacional. Enviaste el parámetro:' + req.params.pais)
})
app.get('/aloha', (req, res) => {
  res.send('Aloha World!')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})