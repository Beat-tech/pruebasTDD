const chai = require('chai');
const fizzbuzz = require('../fizzbuzz');
const boolToWord = require('../bool')
const highAndLow = require('../gap')

// import { expect } from 'chai';
// import { fizzbuzz } from '../fizzbuzz';


describe('Kata FizzBuzz', () => {
  it('Should return a given number as a string', () => {
    chai.expect(fizzbuzz.fizzbuzz(1)).to.eql('1');
    // expect(fizzbuzz(1)).to.eql('1');
  });
  it('Should return FIZZBUZZ if number is divisible by 3 AND 5', () => {
    chai.expect(fizzbuzz.fizzbuzz(15)).to.eql('FIZZBUZZ');
    chai.expect(fizzbuzz.fizzbuzz(30)).to.eql('FIZZBUZZ');
    chai.expect(fizzbuzz.fizzbuzz(45)).to.eql('FIZZBUZZ');
  });
  it('Should return FIZZ if number is divisible by 3', () => {
    chai.expect(fizzbuzz.fizzbuzz(6)).to.eql('FIZZ');
    chai.expect(fizzbuzz.fizzbuzz(9)).to.eql('FIZZ');
    chai.expect(fizzbuzz.fizzbuzz(12)).to.eql('FIZZ');
  });
  it('Should return BUZZ if number is divisible by 3', () => {
    chai.expect(fizzbuzz.fizzbuzz(5)).to.eql('BUZZ');
    chai.expect(fizzbuzz.fizzbuzz(10)).to.eql('BUZZ');
    chai.expect(fizzbuzz.fizzbuzz(25)).to.eql('BUZZ');
  });

})

describe('Kata Bool', () => {
  it('Should return Yes if boolean is True', () => {
    chai.expect(boolToWord.boolToWord(4 > 3)).to.eql('Yes');
    chai.expect(boolToWord.boolToWord(7 > 3)).to.eql('Yes');
    chai.expect(boolToWord.boolToWord(8 > 3)).to.eql('Yes');
  });
  it('Should return No if boolean is False', () => {
    chai.expect(boolToWord.boolToWord(4 < 3)).to.eql('No');
    chai.expect(boolToWord.boolToWord(7 < 3)).to.eql('No');
    chai.expect(boolToWord.boolToWord(9 < 3)).to.eql('No');
  });
});

describe('Kata Gap', () => {
  it('Should return Highest and Lowest number from a String', () => {
    chai.expect(highAndLow.highAndLow('1 2 3 4 5')).to.eql('5 1');
    chai.expect(highAndLow.highAndLow('1 2 -3 4 5')).to.eql('5 -3');
    chai.expect(highAndLow.highAndLow('1 9 3 4 -5')).to.eql('9 -5');
  });
});


var assert = require('assert');
const { expect } = require('chai');
describe('Array', function () {
  describe('#indexOf()', function () {
    it('should return -1 when the value is not present', function () {
      assert.equal([1, 2, 3].indexOf(4), -1);
    });
  });
});